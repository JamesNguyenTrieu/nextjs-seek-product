import React, { Component } from 'react';
import * as productActions from "../../../redux/actions/ProductActions";
import * as cartActions from "../../../redux/actions/CartActions";
import { connect } from 'react-redux';
import Router from 'next/router';
import NavigationBar from '../../../components/NavigationBar/NavigationBar';
import '../../../components/DetailedProduct/DetailedProduct.scss';
import { getCookieFromBrowser } from '../../../redux/utils/cookies';
import Swal from 'sweetalert2';
import Link from 'next/link';

export class productID extends Component {
    constructor(props, context) {
        super(props, context);
        
        this.state = {
            productID: "",
            quantity: 1
        }
    }

    componentDidMount() {
        let proID =  Router.query.productID;
        if (proID) {
            this.setState({
                ...this.state,
                productID: proID
            });
        }        
        this.props.fetchOneProduct(proID);
    }

    changeDetailedProduct = (productID) => {
        this.props.fetchOneProduct(productID);
    }

    changeImage = (url) => {
        if (url === null) {
            url = "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png";
        }
        document.getElementById('avatar').src=url;
    }

    addToCart = () => {
        let cookie_token = getCookieFromBrowser('token');
        if ( cookie_token === null || cookie_token === undefined || cookie_token === "") {
            Router.push("/login");
        }
        else {
            this.props.propAddToCart(this.state.productID, this.state.quantity);
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Added to cart successfully!',
                showConfirmButton: false,
                timer: 1000
            });
        }
    }

    onQuantityChange = (e) => {
        if (e.target.value !== this.state.quantity) {
            if (e.target.value === null || e.target.value === "" || parseInt(e.target.value) < 1) {
                this.setState({
                    ...this.state,
                    quantity: 1
                });
            }
            else if (this.props.product.product) {
                if (parseInt(e.target.value) >= this.props.product.product.in_stock) {
                    this.setState({
                        ...this.state,
                        quantity: this.props.product.product.in_stock
                    });
                }
                else {
                    this.setState({
                        ...this.state,
                        quantity: parseInt(e.target.value)
                    });            
                }
            }            
        }
    }
    
    render() {
        var productFeeds = () => {
            if (this.props.product.feeds) {
                return (
                    <div className="small">
                        {
                            this.props.product.feeds.length ?
                            this.props.product.feeds.map(feed => {
                                return (
                                    <div key={feed.id} className="item">
                                        <img onClick={() => this.changeImage(feed.url)} src={feed.url ? feed.url : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} className="img-responsive" alt="" />
                                    </div>
                                )
                            }):
                            null
                        }
                    </div>
                )
            }
        };
        var recommendation = () => {
            if (this.props.product.other_products) {
                return (
                    <div className="large">
                        {
                            this.props.product.other_products.length ?
                            this.props.product.other_products.map(pro => {
                                return (
                                    <Link key={pro.id} href="/products/details/[productID]" as={`/products/details/${pro.id}`}>
                                        <div className="item" onClick={() => this.changeDetailedProduct(pro.id)}> 
                                                <img src={pro.image ? pro.image : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} className="img-responsive" alt="" />
                                                <h5>{pro.name.length < 18 ? pro.name : `${pro.name.substring(0, 18)}...`}</h5>
                                                <h4>${pro.price}</h4>
                                        </div>
                                    </Link>
                                )
                            }):
                            null
                        }
                    </div>
                )
            }
        }
        var isOutOfStock = () => {
            if (this.props.product.product ) {
                if (this.props.product.product.in_stock > 0 ) {
                    return (<button type="button" onClick={this.addToCart} className="btn btn-danger">Add to cart</button>);
                }
            }
            return (<button type="button" className="btn btn-danger disabled" disabled>Out of stock</button>);
        }
        var result = () => {
            console.log(this.props.product);
            
            if (this.props.product) {
                if (this.props.product.product) {
                    return (
                        <div className='container detail-container'>
                            <div className="row hang">
                                <div className="col-xs-12 col-sm-5 col-md-4 col-lg-4 image">
                                    <img id="avatar" src={this.props.product.product && this.props.product.product.image ? this.props.product.product.image : "https://tuongtoanhhong.com/wp-content/themes/mytheme/public/images/empty-img.png"} className="img-responsive" alt="" />
                                    {
                                        productFeeds()
                                    }
                                </div>
                                
                                <div className="col-xs-12 col-sm-7 col-md-6 col-lg-6 info">

                                    <table>
                                        <tbody>
                                            <tr>
                                                <th colSpan="2">
                                                    <h3 className='name'>{this.props.product.product ? this.props.product.product.name : ""}</h3>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th className="fixed">Price:</th>
                                                <th>
                                                    <h4>${this.props.product.product ? this.props.product.product.price : ""}</h4>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th className="fixed">Amount:</th>
                                                <th>
                                                    <input type="number" className="form-control" onChange={this.onQuantityChange} defaultValue="1" min="1" max={this.props.product.product ? this.props.product.product.in_stock : 100} />
                                                </th>
                                            </tr>
                                            <tr className='btn-tr'>
                                                <td colSpan="2">
                                                    {
                                                        isOutOfStock()
                                                    }
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3 className="rec-text">Related Products:</h3>
                                    <div className="recommended">
                                        {
                                            recommendation()
                                        }
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    )
                }
            }
            return (
                <div className="not-found">
                    <img src="https://www.iamqatar.qa/assets/images/no-products-found.png" className="img-responsive" alt="" />
                </div>
            )
        }
            
        return (
            <div className='details'>
                <NavigationBar/>
                {result()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        product: state.productReducer.detailedProduct
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchOneProduct: (pID) => {
            dispatch(productActions.fetchOneProduct(pID));
        },
        propAddToCart: (product, amount) => {
            dispatch(cartActions.actAddToCart(product, amount))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(productID);
