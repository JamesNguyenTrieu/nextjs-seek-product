import React from 'react'
import NavigationBar from '../../../components/NavigationBar/NavigationBar'
import MyProducts from '../../../components/MyProducts/MyProducts'

export default function myProductPage() {
    return (
        <div>
            <NavigationBar />
            <MyProducts />
        </div>
    )
}
