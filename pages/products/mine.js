import React, { Component } from 'react'
import NavigationBar from '../../components/NavigationBar/NavigationBar'
import MyProducts from '../../components/MyProducts/MyProducts'

class mine extends Component {
    render() {
        return (
            <div>
                <NavigationBar />
                <MyProducts />
            </div>
        )
    }
}

export default mine
