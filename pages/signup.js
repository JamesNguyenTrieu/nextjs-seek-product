import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../redux/actions/AuthActions';
import initialize from '../redux/utils/initialize';
import Link from "next/link";
import { CallSignup, CallLogin } from '../apiCallers/Authorization';
import Swal from 'sweetalert2';
import EmptyNavBar from '../components/NavigationBar/EmptyNavBar';
import '../components/FormFormat/form.scss';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmedPassword: ''
    }

    this.onFirstNameChange = this.onFirstNameChange.bind(this);
    this.onLastNameChange = this.onLastNameChange.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onPasswordConfirming = this.onPasswordConfirming.bind(this);

    this.onSubmit = this.onSubmit.bind(this);
  }

  static getInitialProps(ctx) {
    initialize(ctx);
  }

    labelStyle = {
        float: "left"
    }

    onFirstNameChange = (e) => {
        this.setState({
            ...this.state,
            firstName: e.target.value
        });
    }

    onLastNameChange = (e) => {
        this.setState({
            ...this.state,
            lastName: e.target.value
        });
    }

    onEmailChange = (e) => {
        this.setState({
            ...this.state,
            email: e.target.value
        });
    }

    onPasswordChange = (e) => {
        this.setState({
            ...this.state,
            password: e.target.value
        });
    }

    onPasswordConfirming = (e) => {
        this.setState({
            ...this.state,
            confirmedPassword: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();
        CallSignup(this.state.firstName, this.state.lastName, this.state.email, this.state.password, this.state.confirmedPassword)
        .then(suc => {
            if (suc !== undefined) {                
                CallLogin(this.state.email, this.state.password)
                .then(res => {
                    Swal.fire({
                        position: 'top',
                            icon: 'success',
                            title: 'Signed up successfully!',
                            showConfirmButton: false,
                            timer: 2000
                    });
                    this.props.Authenticate(res.data.token, this.state.email);
                });
            }
        }, err => {
            if (err.response.request) {
                if (err.response.request.response) {
                    let mes = JSON.parse(err.response.request.response);
                    if (mes.email) {
                        Swal.fire({
                            position: 'top',
                            icon: 'error',
                            title: 'Invalid email!',
                            text: `${mes.email[0]}! Please try again.`
                        });
                    }
                    else if (mes.password) {
                        Swal.fire({
                            position: 'top',
                            icon: 'error',
                            title: 'Invalid password!',
                            text: `${mes.password[0]}! Please try again.`
                        });
                    }
                    else if (mes.confirm_password) {
                        Swal.fire({
                            position: 'top',
                            icon: 'error',
                            title: 'Invalid confirmed password!',
                            text: `${mes.confirm_password[0]}! Please try again.`
                        });
                    }
                }
            }
        });
    }

  render() {
      var style_center = {
          textAlign: 'center'
      }
      var rowStyle = {
        display: 'flex',
        justifyContent: 'center'
      }
    return (
        <div>
            <EmptyNavBar/>
            <div className="container form-container">
                <div className="row" style={rowStyle}>
                    <div className="col-xs-11 col-sm-6 col-md-5 col-lg-4 main">
                        <div className="panel panel-warning">
                                <div className="panel-heading">
                                    <legend className="panel-title" style={style_center}>
                                        <h3>Signup</h3>
                                    </legend>
                                </div>
                                <div className="panel-body">                                
                                    <form onSubmit={this.onSubmit}>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>First Name:</label>
                                            <input type="text" className="form-control" onChange={this.onFirstNameChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Last Name:</label>
                                            <input type="text" className="form-control" onChange={this.onLastNameChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Email:</label>
                                            <input type="email" className="form-control" onChange={this.onEmailChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Password:</label>
                                            <input type="password" className="form-control" onChange={this.onPasswordChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label style={this.labelStyle}>Confirmed password:</label>
                                            <input type="password" className="form-control" onChange={this.onPasswordConfirming}/>
                                        </div>

                                        <div style={style_center}>
                                            <button type="submit" className="btn btn-success">Create my account</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <button type="reset" className="btn btn-danger">Reset</button>
                                        </div>
                                    </form>
                                    <br></br>
                                    
                                    <div style={style_center}>
                                        <Link href='/login'>
                                            <a>You've had an account? Click here to sign in!</a>
                                        </Link>
                                    </div>
                                    
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default connect(
  state => state,
  actions
)(Signup);