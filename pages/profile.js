import React, { Component } from 'react'
import Profile from '../components/Profile/Profile'
import NavigationBar from '../components/NavigationBar/NavigationBar'

export class profile extends Component {
    render() {
        return (
            <div>
                <NavigationBar/>
                <Profile/>
            </div>
        )
    }
}

export default profile
