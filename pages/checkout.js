import React, { Component } from 'react';
import Payment from "../components/CheckOut/Payment";
import EmptyNavBar from '../components/NavigationBar/EmptyNavBar';

export class checkout extends Component {
  render() {
    return (
      <div>
        <EmptyNavBar/>
        <Payment/>
      </div>
      
    )
  }
}

export default checkout
