import Document, { Head, Main, NextScript } from "next/document";

export default class MyDocument extends Document {
    render() {
      return (
        <html>
          <Head>
            {/* Latest compiled and minified CSS & JS */}
            {/* <link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"></link> */}
            <link rel="shortcut icon" type="image/x-icon" href="https://pngimg.com/uploads/letter_g/letter_g_PNG60.png" />
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossOrigin="anonymous"/>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossOrigin="anonymous"></link>
            {/* bootstrap carousel */}
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
          </Head>
          <body>
            <Main />
            <NextScript />

            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossOrigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossOrigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossOrigin="anonymous"></script>

            {/* Font Awesome */}
            <script src="https://kit.fontawesome.com/912079eddc.js" crossOrigin="anonymous"></script>
            {/* stripe */}
            <script id="stripe-js" src="https://js.stripe.com/v3/" async></script>
            {/* bootstrap carousel */}
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
            {/* Google map */}
            <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAfXQTSXB5jY1hg3Rw_odCpcId_67RQKbU"></script>
          </body>
        </html>
      );
    }
  }