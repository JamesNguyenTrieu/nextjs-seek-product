import React, { Component } from 'react';
import Product from "../components/Product/Product";
import { connect } from 'react-redux';
import { getCookieFromBrowser } from '../redux/utils/cookies';
import * as authActions from "../redux/actions/AuthActions";

export class Home extends Component {
  componentDidMount() {
    document.title = 'GLAD';
    if (getCookieFromBrowser('token') == null || getCookieFromBrowser('user') == null) {
      this.props.signOut();
    }
  }

  render() {
    return (
      <Product/>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      signOut: () => {
          dispatch(authActions.Deauthenticate());
      }, 
  }
}

export default connect(state => state, mapDispatchToProps)(Home);