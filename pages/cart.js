import React, { Component } from 'react'
import Cart from '../components/Cart/Cart'
import NavigationBar from '../components/NavigationBar/NavigationBar'

export class cart extends Component {
    render() {
        return (
            <div>
                <NavigationBar/>
                <Cart/>
            </div>
        )
    }
}

export default cart
