import { connect } from 'react-redux';
import * as actions from '../redux/actions/AuthActions';
import initialize from '../redux/utils/initialize';
import Link from "next/link";
import { Component } from 'react'
import Swal from 'sweetalert2';
import { CallLogin } from '../apiCallers/Authorization';
import EmptyNavBar from '../components/NavigationBar/EmptyNavBar';
// import "../components/Authentication/auth.scss";
import "../components/FormFormat/form.scss";

class Signin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };

    this.getEmail = this.getEmail.bind(this);
    this.getPassword = this.getPassword.bind(this);
    this.onLogIn = this.onLogIn.bind(this);
  }

  static getInitialProps(ctx) {
    initialize(ctx);
  }

  getEmail = (e) => {
    this.setState({
        ...this.state,
        email: e.target.value
    });
  }

  getPassword = (e) => {
    this.setState({
        ...this.state,
        password: e.target.value
    });
  }

  async onLogIn(e) {
    e.preventDefault();
    CallLogin(this.state.email, this.state.password)
    .then(res => {
      if (res !== undefined) {
          Swal.fire({
              position: 'top',
              icon: 'success',
              title: 'Logged in successfully!',
              showConfirmButton: false,
              timer: 1000
          });
          this.props.Authenticate(res.data.token, this.state.email);
      }
    }, err => {
        let mes = JSON.parse(err.response.request.response);
        if (mes.email) {
            Swal.fire({
                position: 'top',
                icon: 'error',
                title: 'Wrong credentials!',
                text: `${mes.email[0]}! Please try again.`
            });
        }
        else if (mes.password) {
            Swal.fire({
                position: 'top',
                icon: 'error',
                title: 'Invalid password!',
                text: `${mes.password[0]}! Please try again.`
            });
        }
    });
  }

  centeringStyle = () => {
    return {
      display: "flex",
      justifiedContent: "space-between"
    }
  }

  render() {
    var style_center = {
        textAlign: 'center'
    }
    var rowStyle = {
      display: 'flex',
      justifyContent: 'center'
    }
    return (
      <div>
        <EmptyNavBar/>
        <br></br>
        <div className="container form-container">
          <div className="row" style={rowStyle}>
              <div className="col-xs-11 col-sm-6 col-md-5 col-lg-4 main">
                  <div className="panel panel-warning">
                        <div className="panel-heading">
                              <legend className="panel-title" style={style_center}>
                                  <h3>Login</h3>
                              </legend>
                        </div>
                        <div className="panel-body">
                              
                              <form onSubmit={this.onLogIn}>                                    
                                  <div className="form-group">
                                      <input type="email" className="form-control" placeholder="Email" required onChange={this.getEmail}/>
                                  </div>
                                  <div className="form-group">
                                      <input type="password" className="form-control" placeholder="Password" required onChange={this.getPassword}/>
                                  </div>

                                  <div className='buttons' style={{textAlign: "center"}}>
                                    <button type="submit" className="btn btn-success">Log in</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="reset" className="btn btn-danger">Reset</button>
                                  </div>
                              </form>
                              <br></br>
                              <div style={style_center}>
                                  <Link href="/signup">
                                    <a>Not had an account yet? Sign up here!</a>
                                </Link>
                              </div>
                              
                        </div>
                  </div>
                  
              </div>
          </div>
        </div>
    </div>
    );
  }
}

// const mapDispatchToProps = (dispatch) => {
//   return {
//     Authenticate: (token) => {
//       dispatch(actions.Authenticate(token));
//     }
//   }
// }

export default connect(
  state => state,
  // mapDispatchToProps
  actions
)(Signin);