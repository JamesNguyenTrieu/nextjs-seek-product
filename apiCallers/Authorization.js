import { API_URL, headers } from "./config";
import callAPIWithHeaders from "./configWithHeaders";
import Axios from "axios";
import { getCookieFromBrowser } from "../redux/utils/cookies";

export const CallLogin = (email, password) => {
    let body = {
        email,
        password
    };
    return Axios.post(`${API_URL}/user/login/`, body, null);
}

export const CallSignup = (first_name, last_name, email, password, confirm_password) => {
    let body = {
        first_name,
        last_name,
        email,
        password,
        confirm_password
    };
    return Axios.post(`${API_URL}/api/auth/register/`, body, null);
}

export const GetProfile = () => {
    return callAPIWithHeaders(`api/auth/profile`, 'GET', null, headers(getCookieFromBrowser("token")));
}

export const UpdateProfile = (body) => {
    return callAPIWithHeaders(`api/auth/profile`, 'PUT', body, headers(getCookieFromBrowser("token")));
}