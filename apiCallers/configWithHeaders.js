import axios from "axios";
import { API_URL } from "./config";

export default function callAPIWithHeaders(endpoint, method = 'GET', body, headers) {
    return axios({
        method: method,
        url: `${API_URL}/${endpoint}`,
        data: body,
        headers
    })
}