import callAPIWithHeaders from "./configWithHeaders";
import Axios from "axios";
import { API_URL } from "./config";
import { getCookieFromBrowser } from "../redux/utils/cookies";

export const headers = (token) => {
    return {
        "Content-Type": "application/json",
        "Authorization": `JWT ${token}`
    }
};

export const GetCart = () => {
    return callAPIWithHeaders('api/user/cart/view-cart/', 'GET', null, headers(getCookieFromBrowser("token")));
}

export const AddToCartCaller = (product, amount) => {
    let body = {
        product,
        amount
    }
    return callAPIWithHeaders('api/user/cart/add-to-cart', 'POST', body, headers(getCookieFromBrowser("token")));
}

export const RemoveFromCart = (productID) => {
    let body = {
        "product": productID
    }
    return callAPIWithHeaders('api/user/cart/remove-from-cart', 'POST', body, headers(getCookieFromBrowser("token")));
}

export const DeleteAllCartItems = () => {
    return callAPIWithHeaders('api/user/cart/clear', 'GET', null, headers(getCookieFromBrowser("token")));
}

export const UpdateCart = (product, amount) => {
    let body = {
        product,
        amount
    }
    return callAPIWithHeaders('api/user/cart/update-cart', 'POST', body, headers(getCookieFromBrowser("token")));
}

export const CartPayment = (name, address_line1, token) => {
    let body = {
        option: 2,
        name,
        address_line1,
        token: token
    }
    return Axios({
        method: 'POST',
        url: `${API_URL}/api/user/cart/payment`, 
        data: body, 
        headers: headers(getCookieFromBrowser("token"))
    });
}

export const OrderCreating = (phone, first_name, last_name, charge_id, address_line_1) => {
    let body = {
        email: getCookieFromBrowser("user"),
        phone,
        first_name,
        last_name,
        charge_id, address_line_1
    }
    return Axios({
        method: 'POST',
        url: `${API_URL}/api/user/order/create`,
        data: body, 
        headers: headers(getCookieFromBrowser("token"))
    });
}