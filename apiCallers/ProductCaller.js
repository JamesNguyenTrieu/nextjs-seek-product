import callAPI, { headers, formdataHeaders } from "./config";
import callAPIWithHeaders from "./configWithHeaders";
import { getCookieFromBrowser } from "../redux/utils/cookies";

export const GetProducts = (page) => {
    return callAPI(`api/products/list?page=${page}&page_size=${12}`);
}

export const GetCategoryProducts = (cate, page) => {
    var body = {
        "key_word": "",
        "category": cate
    }
    return callAPI(`api/category/search/?order_by=-id&page=${page}&page_size=12`, 'POST', body);
}

export const GetDetailedProduct = (pID) => {
    return callAPI(`api/products/${pID}/details/`, 'GET', null);
}

export const GetMyProducts = (page) => {
    return callAPIWithHeaders(`api/user/product/my-product/?ordering=id&order=DESC&page=${page}`, 'GET', null, headers(getCookieFromBrowser('token')));
}

export const GetProductGroups = () => {
    return callAPIWithHeaders(`api/user/productgroup/`, 'GET', null, headers(getCookieFromBrowser('token')));
}

export const CreateProduct = (site, formData) => {
    return callAPIWithHeaders(`api/user/product/?site=${site}`, 'POST', formData, formdataHeaders(getCookieFromBrowser('token')));
}

export const UpdateProduct = (pID, formData) => {
    return callAPIWithHeaders(`api/user/product/${pID}`, 'PUT', formData, formdataHeaders(getCookieFromBrowser('token')));
}

export const GetCompanies = () => {
    return callAPIWithHeaders(`api/user/company/`, 'GET', null, headers(getCookieFromBrowser('token')));
}

export const DeleteProduct = (pID) => {
    return callAPIWithHeaders(`api/user/product/${pID}`, 'DELETE', null, headers(getCookieFromBrowser('token')));
}

export const GetMyDetailedProduct = (pID) => {
    return callAPIWithHeaders(`api/user/product/${pID}`, 'GET', null, headers(getCookieFromBrowser('token')));
}