import axios from "axios";
export const API_URL = 'http://172.104.50.113:886';
// export const API_URL = 'https://cors-anywhere.herokuapp.com/http://172.104.50.113:886';

export default function callAPI(endpoint, method = 'GET', body) {
    return axios({
        method: method,
        url: `${API_URL}/${endpoint}`,
        data: body
    }).catch(err => {
        console.log(err);     
    })
}


export const headers = (token) => {
    return {
        "Content-Type": "application/json",
        "Authorization": `JWT ${token}`
    }
};

export const formdataHeaders = (token) => {
    return {
        "Content-Type": "multipart/form-data",
        "Authorization": `JWT ${token}`
    }
};