import callAPI from "./config";

const GetAllCategories = () => {
    return callAPI(`api/category`);
}

export default GetAllCategories;