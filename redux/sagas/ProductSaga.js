import * as types from "../constants/ProductsTypes";
import { GetProducts, GetCategoryProducts, GetDetailedProduct } from "../../apiCallers/ProductCaller";
import { takeLatest, put } from 'redux-saga/effects';
import { setCookie } from "../utils/cookies";

function* actFetchProducts({ pageNumber }) {
    let data = {}
    yield GetProducts(pageNumber).then(res => {
        if (res !== undefined) {
            data = res.data;
            setCookie('numberOfProducts', res.data.count);
        }
    });
    yield put({
        type: types.FETCH_PRODUCTS,
        results: data
    });
}

export function* watchGetProducts() {
    yield takeLatest(types.FETCHING_PRODUCTS_CALL, actFetchProducts);
}

function* actFetchProductsByCategory({ category, pageNumber }) {
    let data = {}
    yield GetCategoryProducts(category, pageNumber).then(res => {
        if (res !== undefined) {
            data = res.data;
            setCookie('numberOfProducts', res.data.count);
        }
    });
    yield put({
        type: types.FETCH_PRODUCTS_BY_CATEGORY,
        results: data
    });
}

export function* watchGetProductsByCategory() {
    yield takeLatest(types.FETCHING_PRODUCTS_BY_CATEGORY_CALL, actFetchProductsByCategory);
}

function* actFetchDetailedProduct({ id }) {
    
    let data = {}
    yield GetDetailedProduct(id).then(res => {
        console.log(res);
        
        if (res !== undefined) {
            data = res.data;
        }
    });
    yield put({
        type: types.FETCH_DETAILED_PRODUCT,
        results: data
    });
}

export function* watchGetDetailedProduct() {
    yield takeLatest(types.FETCHING_DETAILED_PRODUCT_CALL, actFetchDetailedProduct);
}