import { GetCart, AddToCartCaller, RemoveFromCart, DeleteAllCartItems, UpdateCart, CartPayment2 } from "../../apiCallers/CartCaller";
import * as types from "../constants/CartTypes";
import { takeLatest, put } from "redux-saga/effects";

function* actGetCart() {
    let data = {}
    yield GetCart().then(res => {
        if (res) {
            data = res.data;
        }
    });
    yield put({
        type: types.FETCH_CART,
        results: data,
    });
}

export function* watchGetCart() {
    yield takeLatest(types.FETCH_CART_CALL, actGetCart);
}

function* actDeleteOneItem({ productID }) {
    let data = {}
    yield RemoveFromCart(productID).then(res => {
        if (res) {
            data = res.data;
        }
    });
    yield put({
        type: types.DELETE_ONE_ITEM,
        results: data,
    });
}

export function* watchDeleteOneItem() {
    yield takeLatest(types.DELETE_ONE_ITEM_CALL, actDeleteOneItem);
}

function* actAddToCart({ productID, amount }) {
    let data = {};
    yield AddToCartCaller(productID, amount).then(res => {
        if (res) {
            data = res.data;
        }
    });
    yield put({
        type: types.ADD_TO_CART,
        results: data,
    });
}

export function* watchAddToCart() {
    yield takeLatest(types.ADD_TO_CART_CALL, actAddToCart);
}

function* actDeleteCart() {
    let data = {};
    yield DeleteAllCartItems().then(res => {
        if (res) {
            data = res.data;
        }
    });
    yield put({
        type: types.DELETE_CART,
        results: data
    });
}

export function* watchDeleteCart() {
    yield takeLatest(types.DELETE_CART_CALL, actDeleteCart);
}

function* actUpdateCart({ productID, amount }) {    
    let data = {};
    yield UpdateCart(productID, amount).then(res => {
        if (res) {
            data = res.data;
        }
    });
    yield put({
        type: types.UPDATE_CART,
        results: data
    });
}

export function* watchUpdateCart() {
    yield takeLatest(types.UPDATE_CART_CALL, actUpdateCart);
}

function* actCheckOut() {
    yield put({
        type: types.CHECK_OUT
    });
}

export function* watchCheckOut() {
    yield takeLatest(types.CHECK_OUT_CALL, actCheckOut);
}