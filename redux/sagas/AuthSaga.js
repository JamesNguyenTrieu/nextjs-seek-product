import { takeLatest, put } from 'redux-saga/effects';
import * as types from "../constants/AuthTypes";

function* actLogIn( {token, user} ) {
    yield put({
        type: types.LOGIN,
        token,
        user
    });
}

export function* watchLogin() {
    yield takeLatest(types.LOGIN_CALL, actLogIn);
}

function* actLogOut() {
    yield put({
        type: types.LOGOUT,
    });
}

export function* watchLogOut() {
    yield takeLatest(types.LOGOUT_CALL, actLogOut);
}