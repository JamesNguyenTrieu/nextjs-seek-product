import * as types from "../constants/CategoryTypes";
import { takeLatest, put } from 'redux-saga/effects';
import GetAllCategories from "../../apiCallers/CategoryCaller";

function* actGetCategories() {
    let data = {}
    yield GetAllCategories().then(res => {
        if (res) {
            data = res.data.results
        }
    });
    yield put({
        type: types.ACT_FETCH_CATEGORIES,
        categories: data
    });
}

export function* watchGetCategories() {
    try {
        yield takeLatest(types.FETCH_ALL_CATEGORIES, actGetCategories);
    } catch { err => console.log(err);
     }
}