import { all } from 'redux-saga/effects';
import { watchGetCategories } from './CategorySaga';
import { watchGetProducts, watchGetProductsByCategory, watchGetDetailedProduct } from './ProductSaga';
import { watchLogin, watchLogOut } from './AuthSaga';
import { watchGetCart, watchDeleteOneItem, watchAddToCart, watchDeleteCart, watchUpdateCart, watchCheckOut } from "./CartSaga";

export default function* rootSaga() {
    yield all([
        watchGetCategories(),
        watchGetProducts(),
        watchGetProductsByCategory(),
        watchGetDetailedProduct(),
        watchLogin(),
        watchLogOut(),
        watchGetCart(),
        watchDeleteOneItem(),
        watchAddToCart(),
        watchDeleteCart(),
        watchUpdateCart(),
        watchCheckOut()
    ]);
}