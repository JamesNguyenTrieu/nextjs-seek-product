import * as types from '../../redux/constants/AuthTypes';

const initialState = {
  token: null,
  user: null,
};

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN:
      state.token = action.token,
      state.user = action.user
      return {
        ...state,
        token: state.token,
        user: state.user
      };
    case types.LOGOUT:
      state.token = null;
      return {
        ...state,
        token: state.token
      };
    default:
      return state;
  }
};

export default AuthReducer;