import * as types from "./../constants/CategoryTypes";
import { setCookie } from "../utils/cookies";

var initialState = {
    categories: []
}

const categoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ACT_FETCH_CATEGORIES:
            state.categories = action.categories;
            setCookie("categories", action.categories);
            return {
                categories: state.categories
            };
        default:
            return state;
    }
}

export default categoryReducer;