import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import categoryReducer from "./CategoryReducer";
import productReducer from "./ProductReducer";
import cartReducer from "./CartReducer";

const rootReducer = combineReducers({
  authentication: AuthReducer,
  categoryReducer,
  productReducer,
  cartReducer
});

export default rootReducer;