import * as types from "../constants/ProductsTypes";

var initialState = {
    products: [],
    numberOfProducts: 0,
    detailedProduct: [],
};

const productReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.FETCH_PRODUCTS:            
            if (action.results === null) {
                return {
                    products: [],
                    numberOfProducts: 0
                };
            } 
            state.products = action.results.results;
            state.numberOfProducts = action.results.count;
            return {
                products: [...state.products],
                numberOfProducts: state.numberOfProducts
            };
        case types.FETCH_PRODUCTS_BY_CATEGORY:            
            if (action.results === null) {
                return {
                    products: [],
                    numberOfProducts: 0
                };
            }
            state.products = action.results.results;
            state.numberOfProducts = action.results.count;
            return {
                products: [...state.products],
                numberOfProducts: state.numberOfProducts
            };
        case types.FETCH_DETAILED_PRODUCT:
            console.log(action.results);
            
            state.detailedProduct = action.results;
            return {
                ...state,
                detailedProduct: state.detailedProduct
            }
        default: return state;
    }
}

export default productReducer;