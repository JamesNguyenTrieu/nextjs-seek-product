import * as types from "./../constants/CartTypes";
import { setCookie, removeCookie } from "../utils/cookies";

var initialState = {
    cart: [],
    total: 0
}

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_CART:
            if (action.results === undefined) {
                return {
                    cart: [],
                    total: 0,
                };
            }
            state.cart = action.results.cart_detail;
            state.total = action.results.total;
            setCookie('cart', JSON.stringify(state.cart));
            return {
                cart: [...state.cart],
                total: state.total,
            };
        case types.ADD_TO_CART:
            state.cart = action.results.cart_detail;
            state.total = action.results.total;
            setCookie('cart', JSON.stringify(state.cart));
            return {
                cart: [...state.cart],
                total: state.total
            };
        case types.UPDATE_CART:            
            state.cart = action.results.cart_detail;
            state.total = action.results.total;
            setCookie('cart', JSON.stringify(state.cart));
            return {
                cart: [...state.cart],
                total: state.total
            };
        case types.DELETE_CART:
            state.cart = action.results.cart_detail;
            state.total = action.results.total;
            setCookie('cart', JSON.stringify(state.cart));
            return {
                cart: [...state.cart],
                total: state.total
            }
        case types.DELETE_ONE_ITEM:            
            state.cart = action.results.cart_detail;
            state.total = action.results.total;
            setCookie('cart', JSON.stringify(state.cart));
            return {
                cart: [...state.cart],
                total: state.total
            }
        case types.CHECK_OUT:
            state.cart = [];
            state.total = 0;
            removeCookie('cart');
            return {
                cart: [...state.cart],
                total: state.total
            }
        default:
            return state;
    }
}

export default cartReducer;