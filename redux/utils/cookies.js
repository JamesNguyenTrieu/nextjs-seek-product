import Cookies from 'js-cookie';

export const setCookie = (key, value) => {
  if (process.browser) {
    Cookies.set(key, value, {
      expires: 3,
      path: '/'
    });
  }
};

export const removeCookie = (key) => {
  if (process.browser) {
    Cookies.remove(key, {
      expires: 3,
      path: '/'
    });
  }
};

export const getCookie = (key, req) => {
  return process.browser
    ? getCookieFromBrowser(key)
    : getCookieFromServer(key, req);
};

export const getCookieFromBrowser = key => {
  return Cookies.get(key);
};

const getCookieFromServer = (key, req) => {
  if (!req.headers.Cookies) {
    return undefined;
  }
  const rawCookie = req.headers.Cookies
    .split(';')
    .find(c => c.trim().startsWith(`${key}=`));
  if (!rawCookie) {
    return undefined;
  }
  return rawCookie.split('=')[1];
};