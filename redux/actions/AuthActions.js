import * as types from '../constants/AuthTypes';
import { setCookie, removeCookie } from '../../redux/utils/cookies';
import Router from 'next/router';

export const Authenticate = (token, user) => {
  return dispatch => {
    setCookie("token", token);
    setCookie("user", user);
    Router.push("/");
    dispatch ({
      type: types.LOGIN_CALL,
      token, user
    });
  }
}

export const Reauthenticate = (token, user) => {  
  return (dispatch) => {
    dispatch({type: types.LOGIN_CALL, token, user});
  };
};

export const Deauthenticate = () => {
  return (dispatch) => {
    removeCookie('token');
    removeCookie('user');
    dispatch({type: types.LOGOUT_CALL});
  };
};