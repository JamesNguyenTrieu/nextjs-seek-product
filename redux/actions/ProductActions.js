import * as types from '../constants/ProductsTypes';

export const fetchAllProducts = (pageNumber) => {
    return {
        type: types.FETCHING_PRODUCTS_CALL,
        pageNumber
    }
}

export const fetchProductsByCategory = (cat, page) => {
    return {
        type: types.FETCHING_PRODUCTS_BY_CATEGORY_CALL,
        category: cat,
        pageNumber: page
    }
}

export const fetchOneProduct = (id) => {
    return {
        type: types.FETCHING_DETAILED_PRODUCT_CALL,
        id      
    }
}