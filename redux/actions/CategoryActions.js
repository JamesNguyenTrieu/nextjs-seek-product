import * as types from "./../constants/CategoryTypes";
// import GetAllCategories from "../../apiCallers/CategoryCaller";

// export const fetchAllCategories = () => {
//     return (dispatch) => {
//         return GetAllCategories().then(res => {
//             if (res) {
//                 if (res) {
//                     dispatch({
//                         type: types.FETCH_ALL_CATEGORIES,
//                         categories: res.data.results
//                     });
//                 }
//             }
//         });
//     }    
// }

export const fetchCategories = () => {
    return {
        type: types.FETCH_ALL_CATEGORIES
    }
}