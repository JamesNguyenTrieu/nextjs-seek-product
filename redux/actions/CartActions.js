import * as types from "./../constants/CartTypes";

export const fetchCart = () => {
    return {
        type: types.FETCH_CART_CALL,
    }
}

export const updateCartItemQuantity = (productID, amount) => {
    return {
        type: types.UPDATE_CART_CALL,
        productID,
        amount
    }
}

export const deleteCart = () => {
    return {
        type: types.DELETE_CART_CALL
    }
}

export const deleteOneItem = (productID) => {
    return {
        type: types.DELETE_ONE_ITEM_CALL,
        productID
    }
}

export const checkout = () => {
    return {
        type: types.CHECK_OUT_CALL
    }
}

export const actAddToCart = (productID, amount) => {
    return {
        type: types.ADD_TO_CART_CALL,
        productID,
        amount
    }
}