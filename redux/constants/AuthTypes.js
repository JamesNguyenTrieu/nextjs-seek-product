export const LOGIN = 'LOGIN';
export const LOGIN_CALL = 'LOGIN_CALL';
export const SIGNUP = 'SIGNUP';
export const SIGNUP_CALL = 'SIGNUP_CALL';
export const LOGOUT = 'LOGOUT';
export const LOGOUT_CALL = 'LOGOUT_CALL';